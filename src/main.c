/*  gngeo a neogeo emulator
 *  Copyright (C) 2001 Peponas Mathieu
 * 
 *  This program is free software; you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by   
 *  the Free Software Foundation; either version 2 of the License, or    
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <SDL.h>

#include "ym2610/2610intf.h"
#include "video.h"
#include "screen.h"
#include "emu.h"
#include "sound.h"
#include "memory.h"
#include "debug.h"
#include "conf.h"
#include "event.h"
#include "gnutil.h"
#include "resfile.h"
#include "roms.h"

static uint32_t videobuffer[352*256];

int main(int argc, char *argv[]) {
    char *rom_name;

    cf_init(); /* must be the first thing to do */
    cf_init_cmd_line();
    cf_open_file(NULL); /* Open Default configuration file */

    rom_name=cf_parse_cmd_line(argc,argv);
    printf("rom_name: %s\n", rom_name);

    init_sdl();
    init_event();

    if (res_verify_datafile(NULL) == GN_FALSE) {
	printf("Failed resfile verify\n");
	exit(1);
    }

    if (init_game(rom_name)!=GN_TRUE) {
	printf("Can't init %s...\n",rom_name);
        exit(1);
    }

    video_set_buffer(&videobuffer);

    init_sdl_audio();
    pause_audio(0);

    while (1) {
	main_loop();
	screen_update();
    }

    close_game();

    return 0;
}
