#include <SDL.h>
#include <GL/glu.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "gnutil.h"
#include "screen.h"
#include "emu.h"
#include "video.h"
#include "conf.h"

extern uint32_t *vbuf;
static SDL_Window *window;
static SDL_GLContext glcontext;
static GLuint texture_id = 0;

static int screen_init() {
	/* Initialization of some variables */
	conf.res_x = 304;
	conf.res_y = 224;

	if (window != NULL) return GN_TRUE;

	uint32_t sdl_flags = SDL_WINDOW_OPENGL;

	window = SDL_CreateWindow("Gngeo",
				  SDL_WINDOWPOS_UNDEFINED,
				  SDL_WINDOWPOS_UNDEFINED,
				  352, 256,
				  sdl_flags);

	glcontext = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, glcontext);
	SDL_GL_SetSwapInterval(1);

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glViewport(0, 0, 352, 256);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_3D_EXT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 352, 256, 0.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	return GN_TRUE;
}

void screen_update() {
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 352);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 352, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, vbuf);
	glViewport(0, 0, 352, 256);

	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(352, 256);

	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(352, 0.0);

	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(0.0, 0.0);

	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(0, 256);
	glEnd();

	SDL_GL_SwapWindow(window);
}

void init_sdl(void) {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE);
    atexit(SDL_Quit);

    if (screen_init() == GN_FALSE) {
		printf("Screen initialization failed.\n");
		exit(-1);
	}
}
